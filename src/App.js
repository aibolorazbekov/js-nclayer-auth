import logo from './logo.svg';
import './App.css';

import { NCALayerClient } from 'ncalayer-js-client';
import { useEffect } from 'react';


function App() {

  useEffect(() => {
    connectAndSign();
  }, [])

  async function connectAndSign() {
    const ncalayerClient = new NCALayerClient();

    try {
      await ncalayerClient.connect();
    } catch (error) {
      alert(`Не удалось подключиться к NCALayer: ${error.toString()}`);
      return;
    }

    try {
      const clientInfo = await ncalayerClient.getKeyInfo('PKCS12');
      console.log('clientInfo: ', clientInfo)
    } catch (error) {
      if (error.canceledByUser) {
        alert('Действие отменено пользователем.');
      }

      alert(error.toString());
      return;
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
